# FreeBSD’s ABI upgrade scripts project up to -STABLE and even though -CURRENT by SymphonyOS

Those scritps are the simplified scripts, and you should choose ONLY ONE first script to start so, according to your **own wanted FreeBSD version to be upgraded** then run so; and then run the middle script to ``buildworld`` and building kernel within installing, then I’ve also prepared an auto reboot inside the middle script; finally also run the final script to ``installworld`` with some post upgrade cleans and port binary upgrades

## Git cloning mirrors

* https://git.code.sf.net/p/symphony-freebsd/src_upgrade_scripts
    * https://notabug.org/HD_Scanius/SymphonyOS-ABI_src_upgrade_scripts.git
        * https://gitlab.com/hd_scania/SymphonyOS-ABI_src_upgrade_scripts.git

### Why nothing GitHub?
Don’t you still remember they’re m$-acquired? How **sucky** their Windows has been, is also **if and only if** how **sucky** their GitHub will be

## Minimal system recommendations for upgrades

1. AMD Ryzen 5 4600U or its Intel 6C12T replacements
2. 29 GiB RAM (29.8GiB = decimal 32GB)
3. HDMI capable of 1920×1080 screen resolution
4. I also assume you’re on UEFI to run those upgrade scripts, so that when legacy BIOS was gone you won’t be left alone; and don’t worry 6C12T processors are almost always brought with UEFI☺️
